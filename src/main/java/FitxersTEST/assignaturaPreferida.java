package FitxersTEST;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "assignaturaPreferida")
@XmlType(propOrder = { "nom", "horesSetmana"})

public class assignaturaPreferida {

	public assignaturaPreferida() {
		super();
	}

	String codi;
	String nom;
	int horesSetmana;

	@XmlElement(name = "nom")
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@XmlElement(name = "horesSetmana")
	public int getHoresSetmana() {
		return horesSetmana;
	}

	public void setHoresSetmana(int horesSetmana) {
		this.horesSetmana = horesSetmana;
	}

	@XmlAttribute
	public String getCodi() {
		return codi;
	}

	public assignaturaPreferida(String codi, String nom, int horesSetmana) {
		super();
		this.codi = codi;
		this.nom = nom;
		this.horesSetmana = horesSetmana;
	}

	public void setCodi(String codi) {
		this.codi = codi;
	}

	@Override
	public String toString() {
		return "assignatura [codi=" + codi + ", nom=" + nom + ", horesSetmana=" + horesSetmana + "]";
	}

}
