package FitxersTEST;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "classe")
@XmlType(propOrder = { "nom", "alumnes"})

public class classe {

	public classe(String nom, List<alumne> alumnes) {
		super();
		this.nom = nom;
		this.alumnes = alumnes;
	}

	public classe() {
		super();
	}

	String nom;
	List<alumne> alumnes = new ArrayList<>();

	@XmlElement(name = "nom")
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@XmlElementWrapper(name = "alumnes")
	@XmlElement(name = "alumne")
	public List<alumne> getAlumnes() {
		return alumnes;
	}

	public void setAlumnes(List<alumne> alumnes) {
		this.alumnes = alumnes;
	}

	@Override
	public String toString() {
		return "classe [nom=" + nom + ", alumnes=" + alumnes + "]";
	}

}
