package FitxersTEST;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class MainFitxers {

	public static void main(String[] args) throws IOException {

//		String classexml = "classe.xml";
//		String classexmlcopia = "classe_copia.xml";
		String classejson = "classe.json";
		String classejsoncopia = "classe_copia.json";

		// llegirClasseXML(classexml); FUNCIONA
		// llegirClasseICopiar(classexml, classexmlcopia); FUNCIONA
		// afegirVot(classexmlcopia, "Pau", true); FUNCIONA
		// comprovarGuanyador(classexmlcopia);

		// escriureJSON("prova.json");
		// System.out.println(llegirJSON(classejson));
		// copiar(classejson, classejsoncopia);
		// afegirVotJSON(classejsoncopia, "Iker", true);

		// 2.3. Fes una funció que elimini un vot false de tots els alumnes amb
		// una nota mitjana superior a 9.0. En cas que un alumne no tingui vots false,
		// afegeix un vot true. (2.25p) (1.5 punt eliminar el vot false, 0.75 fer el
		// afegir el vot
		// true si no hi ha vots false)

	}

	/**
	 * 
	 * Elimina un vot false de tots els alumnes amb una mitjana superior a 9.0.
	 *
	 * @param path
	 * @throws IOException
	 */

	public static void eliminarVot(String path) throws IOException {
		JsonObject classe = llegirJSON(path);
		for (int i = 0; i < classe.get("alumnes").getAsJsonArray().size(); i++) {
			if (classe.get("alumnes").getAsJsonArray().get(i).getAsJsonObject().get("notaMitja").getAsDouble() > 9.0) {
				
			}
		}
	}

	/**
	 * 
	 * Afegeix un vot a l’alumne amb el nom.
	 * 
	 * @param path
	 * @param nom
	 * @throws IOException
	 */

	public static void afegirVotJSON(String path, String nom, Boolean vot) throws IOException {
		JsonObject classe = llegirJSON(path);
		for (int i = 0; i < classe.get("alumnes").getAsJsonArray().size(); i++) {
			if (classe.get("alumnes").getAsJsonArray().get(i).getAsJsonObject().get("nom").getAsString().equals(nom)) {
				classe.get("alumnes").getAsJsonArray().get(i).getAsJsonObject().get("vots").getAsJsonArray().add(vot);
			}
		}
		escriureJSON(path, classe);
	}

	/**
	 * 
	 * Copiar un JSON a un altre fitxer.
	 * 
	 * @param path
	 * @param copia
	 * @throws IOException
	 */

	public static void copiarJSON(String path, String copia) throws IOException {
		JsonObject classe = llegirJSON(path);
		escriureJSON(copia, classe);

	}

	/**
	 * 
	 * llegeix un json
	 * 
	 * @param path
	 * @throws JsonIOException
	 * @throws JsonSyntaxException
	 * @throws FileNotFoundException
	 */

	public static JsonObject llegirJSON(String path) throws IOException {
		JsonObject obj = null;
		try {
			JsonElement arrel = JsonParser.parseReader(new FileReader(path));
			obj = arrel.getAsJsonObject();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return obj;
	}

	public static void escriureJSON(String path, JsonObject obj) throws IOException {
		try {
			Gson write = new GsonBuilder().setPrettyPrinting().create();
			write.toJson(obj);
			FileWriter fw = new FileWriter(path);
			fw.append(write.toJson(obj));
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * Torna el nom de l'alumne amb més vots true XML FALTA FALSE
	 * 
	 * @param path
	 */

	public static void comprovarGuanyador(String path) {
		File fitxer = new File(path);
		classe classe = null;
		int cntguany = 0;
		int cntactual = 0;
		alumne alumneGuany = new alumne();
		try {
			// Es crea el context indicant la classe arrel
			// hauras de posar la classe adient, clar.
			JAXBContext jaxbContext = JAXBContext.newInstance(classe.class);
			// Es crea un Unmarshaller amb el context de la classe Entrada
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			// Es fa servir el mètode unmarshal, per a obtenir les dades. Sempre s'ha de fer
			// un casteig. fitxerOrigen es una String amb el path al fitxer
			classe = (classe) jaxbUnmarshaller.unmarshal(fitxer);
			for (alumne alumne : classe.alumnes) {
				cntactual = 0;
				for (int i = 0; i < alumne.vots.size(); i++) {
					if (alumne.vots.get(i) == true) {
						cntactual++;
					}
				}
				if (cntactual > cntguany) {
					cntguany = cntactual;
					alumneGuany = alumne;
				}
			}
			System.out.println("El guanyador és " + alumneGuany.nom);

		} catch (JAXBException je) {
			je.printStackTrace();
		}
	}

	/**
	 * 
	 * Afegeix un vot a l'alumne amb el nom passat XML
	 * 
	 * @param path
	 * @param nomAlumne
	 * @param vot
	 */

	public static void afegirVot(String path, String nomAlumne, boolean vot) {
		File fitxer = new File(path);
		classe classe = null;
		try {
			// Es crea el context indicant la classe arrel
			// hauras de posar la classe adient, clar.
			JAXBContext jaxbContext = JAXBContext.newInstance(classe.class);
			// Es crea un Unmarshaller amb el context de la classe Entrada
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			// Es fa servir el mètode unmarshal, per a obtenir les dades. Sempre s'ha de fer
			// un casteig. fitxerOrigen es una String amb el path al fitxer
			classe = (classe) jaxbUnmarshaller.unmarshal(fitxer);
			for (alumne entrada : classe.alumnes) {
				if (entrada.nom.equals(nomAlumne)) {
					entrada.vots.add(vot);
				}
			}
			escriureClasseXML(fitxer, classe);
			System.out.println("Vot afegit!");
		} catch (JAXBException je) {
			je.printStackTrace();
		}
	}

	/**
	 * 
	 * Llegeix una classe i la copia a un altre fitxer XML
	 * 
	 * @param path
	 * @param copia
	 */

	public static void llegirClasseICopiar(String path, String copia) {
		File fitxer = new File(path);
		File fitxerCopia = new File(copia);
		classe c = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(classe.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			c = (classe) jaxbUnmarshaller.unmarshal(fitxer);
			escriureClasseXML(fitxerCopia, c);
		} catch (JAXBException je) {
			je.printStackTrace();
		}
	}

	/**
	 * 
	 * Llegeix una classe XML
	 * 
	 * @param path
	 * @return
	 */

	public static classe llegirClasseXML(String path) {
		File fitxer = new File(path);
		classe classe = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(classe.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			classe = (classe) jaxbUnmarshaller.unmarshal(fitxer);
			System.out.println(classe);
		} catch (JAXBException je) {
			je.printStackTrace();
		}
		return classe;
	}

	/**
	 * 
	 * Escriu una classe XML
	 * 
	 * @param fitxer
	 * @param cl
	 */

	public static void escriureClasseXML(File fitxer, classe cl) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(classe.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(cl, fitxer);
		} catch (JAXBException je) {
			je.printStackTrace();
		}
	}

}
