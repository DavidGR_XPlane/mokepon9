package FitxersTEST;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "alumne")
@XmlType(propOrder = { "nom", "edat", "notaMitja", "assignaturaPreferida", "vots" })

public class alumne {

	public alumne(String nom, int edat, double notaMitja, assignaturaPreferida assignaturaPreferida,
			List<Boolean> vots) {
		super();
		this.nom = nom;
		this.edat = edat;
		this.notaMitja = notaMitja;
		this.assignaturaPreferida = assignaturaPreferida;
		this.vots = vots;
	}

	public alumne() {
		super();
	}

	String nom;
	int edat;
	double notaMitja;
	assignaturaPreferida assignaturaPreferida;
	List<Boolean> vots = new ArrayList<>();

	@XmlElement(name = "nom")
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@XmlElement(name = "edat")
	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	@XmlElement(name = "notaMitja")
	public double getNotaMitja() {
		return notaMitja;
	}

	public void setNotaMitja(double notaMitja) {
		this.notaMitja = notaMitja;
	}

	@XmlElement(name = "assignaturaPreferida")
	public assignaturaPreferida getAssignatura() {
		return assignaturaPreferida;
	}

	public void setAssignatura(assignaturaPreferida assignaturaPreferida) {
		this.assignaturaPreferida = assignaturaPreferida;
	}

	@XmlElementWrapper(name = "vots")
	@XmlElement(name = "vot")
	public List<Boolean> getVots() {
		return vots;
	}

	public void setVots(List<Boolean> vots) {
		this.vots = vots;
	}

	@Override
	public String toString() {
		return "alumne [nom=" + nom + ", edat=" + edat + ", notaMitja=" + notaMitja + ", assignatura="
				+ assignaturaPreferida + ", vots=" + vots + "]";
	}

}