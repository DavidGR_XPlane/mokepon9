package Mokepon9;

public class SexeDiferentException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 778542014853957235L;

	public SexeDiferentException(String message) {
		super(message);
	}

}
