package Mokepon9;

import java.io.Serializable;

public class MokeponCapturat extends Mokepon implements Serializable {

	private static final long serialVersionUID = -4336571270371243501L;

	String nomPosat;
	private String nomEntrenador;
	private int felicitat;
	public static int NumMokeponsCapturats;
	private Objecte objecte;
	public Equipament objecteEquipat;

	public MokeponCapturat(String nom, Tipus tipus) {

		// cridem al constructor idèntic al pare
		super(nom, tipus);
		NumMokeponsCapturats++;
		// la resta de variables les posem nosaltres
		this.nomPosat = nom;
		this.nomEntrenador = "David";
		this.felicitat = 50;

	}
	
	public MokeponCapturat(String nom, int nivell, int hp_max, int hp_actual, Tipus tipus) {

		super(nom, nivell, hp_max, hp_actual);
		NumMokeponsCapturats++;
		this.nomPosat = nom;
		this.nomEntrenador = "David";
		this.felicitat = 50;
		this.setTipus(tipus);

	}

	public MokeponCapturat(String nom, int nivell, int hp_max, int atk, int def, int vel) {

		super(nom, nivell, hp_max, atk, def, vel);
		NumMokeponsCapturats++;
		this.nomPosat = nom;
		this.nomEntrenador = "David";
		this.felicitat = 50;

	}

	public MokeponCapturat(Mokepon mok, String nomPosat, String nomEntrenador) {
		super(mok.getNom(), mok.getNivell(), mok.getHp_max(), mok.getAtk(), mok.getDef(), mok.getVel());
		NumMokeponsCapturats++;
		this.nomEntrenador = nomEntrenador;
		this.nomPosat = nomPosat;
		this.felicitat = 50;
		for (int i = 0; i < mok.getAtacs().size(); i++) {
			this.afegirAtac(mok.getAtacs().get(i));
		}

	}

	public void acariciar() {
		if (this.felicitat < 100) {
			this.felicitat += 10;
			if (this.felicitat > 100) {
				this.felicitat = 100;
			}
		}
	}

	@Override
	public void atacar(Mokepon atacat, int num_atac) {
		if (!this.isDebilitat() && !atacat.isDebilitat()) {
			double modificador = (int) super.efectivitat(this.getAtacs().get(num_atac).tipus, atacat.getTipus());
			int damage = (int) ((((((((2 * this.getNivell()) / 5) + 2) * this.getAtacs().get(num_atac).poder)
					* this.getAtk() / atacat.getAtk()) / 50) + 2) * modificador);
			if (this.felicitat > 50) {
				// atacat.hp_actual -= damage * 1.2;
				atacat.setHp_actual(atacat.getHp_actual() - (int) (damage * 1.2));
				if (atacat.getHp_actual() < 2) {
					System.out.println(atacat.getNom() + " ha quedat debilitat! S'acaba l'atac!");
					atacat.setDebilitat(true);

				}
			} else {
				// atacat.hp_actual -= damage * 0.8;
				atacat.setHp_actual(atacat.getHp_actual() - (int) (damage * 0.8));
				if (atacat.getHp_actual() < 2) {
					System.out.println(atacat.getNom() + " ha quedat debilitat! S'acaba l'atac!");
					atacat.setDebilitat(true);
				}
			}

		} else {
			System.out.println("Un Mokepon debilitat no pot lluitar!");
		}

	}

	public boolean equals(MokeponCapturat mok) {
		if (this == mok)
			return true;
		if (mok == null)
			return false;
		if (getClass() != mok.getClass())
			return false;
		if (this.getNom() == mok.getNom() && this.getSexe() == mok.getSexe() && this.getNivell() == mok.getNivell()
				&& this.getAtk() == mok.getAtk() && this.getDef() == mok.getDef() && this.getVel() == mok.getVel()
				&& this.getHp_max() == mok.getHp_max() && this.getNomEntrenador() == mok.getNomEntrenador()
				&& this.getNomPosat() == mok.getNomPosat()) {
			return true;
		} else {
			return false;
		}

	}

	public void utilitzaObjecte(Objecte objectes) {
		this.objecte.utilitzar(this);
	}

	public String getNomPosat() {
		return nomPosat;
	}

	public String getNomEntrenador() {
		return nomEntrenador;
	}

//	@Override
//	public String toString() {
//		return "MokeponCapturat [nomPosat=" + this.getNomPosat() + ", nomEntrenador=" + this.getNomEntrenador()
//				+ ", felicitat=" + this.felicitat + "]";
//	}

	public Objecte getObjecte() {
		return objecte;
	}

	public void setObjecte(Objecte objecte) {
		this.objecte = objecte;
	}

	public void setObjecteEquipat(Equipament objecteEquipat) {
		this.objecteEquipat = objecteEquipat;
	}
	
	public void setNomEntrenador(String nomEntrenador) {
		this.nomEntrenador = nomEntrenador;
	}

}
