package Mokepon9;

public interface Equipament {
//
	public abstract void equipar(MokeponCapturat mok);

	public abstract void desequipar(MokeponCapturat mok);

	public default boolean potEquipar(MokeponCapturat mok) {
		if (mok.objecteEquipat == null && !mok.isDebilitat()) {
			return true;
		} else {

			return false;
		}
	}

	public default boolean equipMalPosat(MokeponCapturat mok) {
		if (mok.getObjecte() instanceof Equipament) {
			return false;
		} else {
			return true;
		}
	}

}
