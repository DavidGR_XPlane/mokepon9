package Mokepon9;

public class Atac {
//
	String nom;
	double poder;
	Tipus tipus;
	int moviments_maxims;
	int moviments_actuals;

	public Atac(String nom, double poder, Tipus tipus, int moviments_maxims) {

		this.nom = nom;
		this.poder = poder;
		this.tipus = tipus;
		this.moviments_maxims = moviments_maxims;

		if (this.poder < 10) {
			this.poder = 10;
		} else if (this.poder > 100) {
			this.poder = 100;
		}

	}

	public Atac(String nom, Tipus tipus) {
		this.poder = 10;
		this.moviments_maxims = 10;
	}

	@Override
	public String toString() {
		return "Atac [nom=" + nom + ", poder=" + poder + ", tipus=" + tipus + ", moviments_maxims=" + moviments_maxims
				+ ", moviments_actuals=" + moviments_actuals + "]";
	}
	
	

}
