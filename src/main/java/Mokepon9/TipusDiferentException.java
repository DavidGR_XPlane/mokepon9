package Mokepon9;

public class TipusDiferentException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6452814416541966231L;

	public TipusDiferentException(String message) {
		super(message);
	}

}
