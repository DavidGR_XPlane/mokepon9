package Mokepon9;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

public class Mokepon implements Comparable<Object>, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3277546068857332351L;
	private String nom;
	private int nivell;
	private int atk;
	private int def;
	private int vel;
	private int exp;
	private int hp_max;
	private int hp_actual;
	private boolean debilitat = false;
	private Tipus tipus;
	private Sexe sexe;
	private ArrayList<Atac> atacs = new ArrayList<Atac>();
	static String estatic;

	public Mokepon() {
		this.nom = "Sense definir";
		this.nivell = 1;
		this.atk = 1;
		this.def = 1;
		this.vel = 1;

	}

	// ************* Constructors ************* //

	public Mokepon(String nom) {
		this();
		this.nom = nom;
		this.hp_actual = this.hp_max = 10;
		this.sexe = triarSexe();
	}

	public Mokepon(String nom, Tipus tipus) {
		this(nom);
		this.nivell = 1;
		this.atk = 1;
		this.def = 1;
		this.vel = 1;
		this.tipus = tipus;
		this.sexe = triarSexe();
	}

	public Mokepon(String nom, int nivell, int hp_max, int hp_actual) {
		this(nom);
		for (int i = 1; i < nivell; i++) {
			this.pujarNivell();
		}
		this.hp_max = hp_max;
		this.hp_actual = hp_actual;
		this.sexe = triarSexe();
	}

	public Mokepon(String nom, int nivell, int hp_max, int atk, int def, int vel) {
		this(nom);
		this.nom = nom;
		this.nivell = nivell;
		this.hp_max = hp_max;
		this.atk = atk;
		this.def = def;
		this.vel = vel;
		this.hp_actual = hp_max;
		this.sexe = triarSexe();
	}

	// ************* Constructors ************* //

	public MokeponCapturat capturar(String nomPosat, String nomEntrenador) throws Exception {

		if (!(this instanceof MokeponCapturat)) {
			MokeponCapturat mc = new MokeponCapturat(this, nomPosat, nomEntrenador);
			return mc;
		} else {
			throw new MokeponJaCapturatException("g");
		}

	}

	public void diguesNom() {
		System.out.println("El meu nom és: " + this.nom);
	}

	public void atorgarExperiencia(int exp_atorgada) {
		this.exp += exp_atorgada;
		if (this.exp > 100) {
			this.exp -= 100;
			pujarNivell();

		}
	}

	public Sexe triarSexe() {
		Random r = new Random();
		int s = r.nextInt(0, 2);
		if (s == 1) {
			return Sexe.FEMENI;
		} else {
			return Sexe.MASCULI;
		}
	}

	private void pujarNivell() {
		Random r = new Random();
		this.nivell++;
		this.hp_max += r.nextInt(0, 6);
		this.atk += r.nextInt(0, 3);
		this.def += r.nextInt(0, 3);
		this.vel += r.nextInt(0, 3);

	}

	public void afegirAtac(Atac at) {
		if (this.atacs.size() < 2) {
			this.atacs.add(at);
		}
	}

	public void atacar(Mokepon atacat, int num_atac) {
		if (this.debilitat == false && atacat.debilitat == false) {
			double modificador = (int) efectivitat(this.atacs.get(num_atac).tipus, atacat.tipus);
			int damage = (int) ((((((((2 * this.nivell) / 5) + 2) * this.atacs.get(num_atac).poder) * this.atk
					/ atacat.atk) / 50) + 2) * modificador);
			atacat.hp_actual -= damage;
		} else {
			System.out.println("Un Mokepon debilitat no pot lluitar!");
		}

	}

	public boolean equals(Mokepon mok) {
		if (this == mok)
			return true;
		if (mok == null)
			return false;
		if (getClass() != mok.getClass())
			return false;
		if (this.getNom() == mok.getNom() && this.getSexe() == mok.getSexe() && this.getNivell() == mok.getNivell()
				&& this.getAtk() == mok.getAtk() && this.getDef() == mok.getDef() && this.getVel() == mok.getVel()
				&& this.getHp_max() == mok.getHp_max()) {
			return true;
		} else {
			return false;
		}

	}

	public void debilitarse() {
		this.debilitat = true;
	}

	public void curar() {

		this.debilitat = false;
		this.hp_max = 100;

	}

//	public Ou reproduccioDEFINITIU(Mokepon Parella) throws Exception {
//		Random r = new Random();
//		if (this.tipus == Parella.tipus && this.sexe != Parella.sexe && !this.isDebilitat() && !Parella.isDebilitat()) {
//			int a = r.nextInt(0, 2); // per decidir aleatoriament si té el nom del this o de l'altre
//			if (a == 1) {
//				return new Ou(this.nom, this.tipus);
//			} else {
//				return new Ou(Parella.nom, this.tipus);
//			}
//		} else {
//			return null;
//		}
//	}

//	public Ou reproduccio(Mokepon Parella) throws Exception {
//		Random r = new Random();
//		if (this.getTipus() != Parella.getTipus()) {
//			// hauria de tornar un error per que son de diferent tipus
//			throw new TipusDiferentException("Son de diferents tipus!");
//		} else if (this.getSexe() == Parella.getSexe()) {
//			// tornem qualsevol cosa per a provar
//			throw new SexeDiferentException("Son del mateix sexe!");
//		} else if (this.isDebilitat() || Parella.isDebilitat()) {
//			throw new DebilitatsException("Un dels dos Mokepons està debilitat!");
//		} else {
//			int a = r.nextInt(0, 2); // per decidir aleatoriament si té el nom del this o de l'altre
//			if (a == 1) {
//				return new Ou(this.getNom(), this.getTipus());
//			} else {
//				return new Ou(Parella.getNom(), this.getTipus());
//			}
//		}
//	}

//	public Ou reproduccio(Mokepon Parella) throws Exception {
//		Random r = new Random();
//		if (this.getTipus() != Parella.getTipus()) {
//			// hauria de tornar un error per que son de diferent tipus
//			assert this.getTipus() == Parella.getTipus() : "Son de diferents tipus";
//			return null;
//		} else if (this.getSexe() == Parella.getSexe()) {
//			// tornem qualsevol cosa per a provar
//			assert this.getSexe() != Parella.getSexe() : "Son del mateix sexe";
//			return null;
//		} else if (this.isDebilitat() || Parella.isDebilitat()) {
//			assert !this.isDebilitat() || !Parella.isDebilitat() : "Un dels dos està debilitat!";
//			return null;
//		} else {
//			int a = r.nextInt(0, 2); // per decidir aleatoriament si té el nom del this o de l'altre
//			if (a == 1) {
//				return new Ou(this.getNom(), this.getTipus());
//			} else {
//				return new Ou(Parella.getNom(), this.getTipus());
//			}
//		}
//	}

	public Ou reproduccio(Mokepon Parella) {
		Random r = new Random();
		assert this.getTipus() == Parella.getTipus() : "Son de diferents tipus";
		assert this.getSexe() != Parella.getSexe() : "Son del mateix sexe";
		assert !this.isDebilitat() || !Parella.isDebilitat() : "Un dels dos està debilitat!";
		int a = r.nextInt(0, 2); // per decidir aleatoriament si té el nom del this o de l'altre
		if (a == 1) {
			return new Ou(this.getNom(), this.getTipus());
		} else {
			return new Ou(Parella.getNom(), this.getTipus());
		}
	}

	static protected double efectivitat(Tipus atac, Tipus defensa) {
		if (atac == Tipus.FOC && defensa == Tipus.AIGUA || atac == Tipus.AIGUA && defensa == Tipus.PLANTA
				|| atac == Tipus.PLANTA && defensa == Tipus.FOC) {
			return 0.5;
		} else if (atac == Tipus.AIGUA && defensa == Tipus.FOC || atac == Tipus.FOC && defensa == Tipus.PLANTA
				|| atac == Tipus.PLANTA && defensa == Tipus.AIGUA) {
			return 2;
		} else {
			return 1;
		}
	}

	public String getNom() {
		return nom;
	}

	public int getNivell() {
		return nivell;
	}

	public int getAtk() {
		return atk;
	}

	public int getDef() {
		return def;
	}

	public int getVel() {
		return vel;
	}

	public int getExp() {
		return exp;
	}

	public int getHp_max() {
		return hp_max;
	}

	public int getHp_actual() {
		return hp_actual;
	}

	public boolean isDebilitat() {
		return debilitat;
	}

	public Tipus getTipus() {
		return tipus;
	}

	public ArrayList<Atac> getAtacs() {
		return atacs;
	}

	public void setHp_actual(int hp_actual) {
		if (hp_actual < 0) {
			this.hp_actual = 0;
		} else if (hp_actual > this.hp_max) {
			this.hp_actual = this.hp_max;
		} else {
			this.hp_actual = hp_actual;
		}

	}

	public void setDebilitat(boolean debilitat) {
		this.debilitat = debilitat;
	}

	@Override
	public String toString() {
		return "Mokepon [nom=" + this.getNom() + ", nivell=" + this.getNivell() + ", atk=" + this.getAtk() + ", def="
				+ this.getDef() + ", vel=" + this.getVel() + ", exp=" + this.getExp() + ", hp_max=" + this.getHp_max()
				+ ", hp_actual=" + this.getHp_actual() + ", debilitat=" + this.isDebilitat() + ", tipus="
				+ this.getTipus() + ", atacs=" + this.getAtacs() + "]";
	}

	public void setAtk(int atk) {
		this.atk = atk;
	}

	public void setDef(int def) {
		this.def = def;
	}

	public Sexe getSexe() {
		return sexe;
	}

	@Override
	public int compareTo(Object o) {
		Mokepon altreMok = (Mokepon) o;
		if (this.getTipus().ordinal() != altreMok.getTipus().ordinal()) {
			return this.getTipus().ordinal() - altreMok.getTipus().ordinal();
		} else if (!this.getNom().equals(altreMok.getNom())) {
			return this.getNom().compareTo(altreMok.getNom());
		} else if (this.getNivell() != altreMok.getNivell()) {
			return this.getNivell() - altreMok.getNivell();
		} else { // iguales en todo
			return 0;
		}

	}

	public void setTipus(Tipus tipus) {
		this.tipus = tipus;
	}

}
