package Mokepon9;

import java.io.Serializable;

public class Reviure extends Objecte implements Serializable {
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//
	public Reviure(String nom) {
		super(nom);
		
	}

	public void utilitzar(MokeponCapturat mok) {
		if (mok.isDebilitat()) {
			mok.setDebilitat(false);
			mok.setHp_actual(1);
			this.Quantitat --;
		} else {
			System.out.println("El mokepon no està debilitat!");
		}
		
	}

	@Override
	public String toString() {
		return "Reviure [nom=" + nom + ", Quantitat=" + Quantitat + "]";
	}	
	
}
