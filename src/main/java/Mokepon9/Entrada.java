package Mokepon9;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "entrada")
@XmlType(propOrder = { "nom", "tipus", "pes", "evolucions", "localitzacions" })

public class Entrada {



	int id, pes;
	String nom, tipus;
	List<String> localitzacions = new ArrayList<>();
	Evolucions evolucions;

	@XmlElement(name = "evolucions")
	public Evolucions getEvolucions() {
		return evolucions;
	}

	public void setEvolucions(Evolucions evolucions) {
		this.evolucions = evolucions;
	}

	@XmlAttribute
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@XmlElement(name = "nom")
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@XmlElement(name = "tipus")
	public String getTipus() {
		return tipus;
	}
	
	@XmlElementWrapper(name = "localitzacions")
	@XmlElement(name = "localitzacio")
	public List<String> getLocalitzacions() {
		return localitzacions;
	}

	public void setLocalitzacions(List<String> localitzacions) {
		this.localitzacions = localitzacions;
	}

	public void setTipus(String t) {
		this.tipus = t;
	}

	@XmlElement(name = "pes")
	public int getPes() {
		return pes;
	}

	public void setPes(int pes) {
		this.pes = pes;
	}
	
	@Override
	public String toString() {
		return "Entrada [id=" + id + ", nom=" + nom + ", tipus=" + tipus + ", localitzacions=" + localitzacions
				+ ", evolucions=" + evolucions + "]";
	}



}
