package Mokepon9;

public class MokeponJaCapturatException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4158173241343872025L;

	public MokeponJaCapturatException(String message) {
		super(message);
	}
}
