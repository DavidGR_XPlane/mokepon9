package Mokepon10;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Random;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Main {

	static ScannerArreglat sc = new ScannerArreglat();
	static Random r = new Random();

	public static void main(String[] args) throws Exception {

		ArrayList<Mokepon> mokedex = new ArrayList<Mokepon>();
		ArrayList<MokeponCapturat> mokedexCapturats = new ArrayList<MokeponCapturat>();
//
//		// -----------------------------------------------
//		// -----------------------------------------------
//		// MOKEPON 1
//		// -----------------------------------------------
//		// -----------------------------------------------
//
//		// podem crear un nou Mokepon perque existeix la classe
//		// i ara es un nou tipus de variable
//
//		// el new Mokepon() es el seu constructor
//		Mokepon mikachu = new Mokepon("Mikachu", Tipus.FOC);
//		// podem accedir als seus atributs interns amb
//		// el punte
//		mikachu.nom = "Mikachu";
//		// també podem accedir als seus mètodes
//		// interns amb el punt
//		mikachu.diguesNom();
//		System.out.println(mikachu.nivell);
//
//		Mokepon missingNo = new Mokepon();
//		System.out.println(missingNo.nom);
//		Mokepon marmander = new Mokepon("Marmander");
//		System.out.println(marmander.nom + " " + marmander.atk);
//		marmander.pujarNivell();
//		System.out.println(marmander.nivell);
//
//		System.out.println("-----------------");
//		Mokepon marman = new Mokepon("Marman", Tipus.FOC);
//		System.out.println(marmander.nom + " " + marmander.atk);
//		if (marman.tipus == Tipus.FOC) {
//			System.out.println("Marman es de Foc!");
//
//		}
//
//	Atac Rasengan = new Atac("Rasengan", 70, Tipus.FOC, 12);
//	Atac Alcala = new Atac("Alcala", 90, Tipus.PLANTA, 12);
//
//		mikachu.afegirAtac(Alcala);
//		mikachu.afegirAtac(Rasengan);
//		marmander.afegirAtac(Alcala);
//		marmander.afegirAtac(Rasengan);
//
//		System.out.println();
//		System.out.println("Mikachu té aquests dos atacs!");
//		System.out.println();
//		System.out.println(mikachu.atacs.get(0).nom);
//		System.out.println(mikachu.atacs.get(1).nom);
//
//		System.out.println("Abans de la batalla...");
//		System.out.println("Mikachu té hp " + mikachu.hp_actual);
//		System.out.println("Marman té hp " + marman.hp_actual);
//
//		mikachu.atacar(marmander, 1);
//		System.out.println("Mikachu ha atacat a Marman!");
//		System.out.println();
//		System.out.println("Mikachu té hp " + mikachu.hp_actual);
//		System.out.println("Marman té hp " + marman.hp_actual);
//		marmander.atacar(mikachu, 1);
//		System.out.println("Marman ha atacat a Marman!");
//
//		System.out.println("Marman té hp " + marman.hp_actual);
//		System.out.println("Mikachu té hp " + mikachu.hp_actual);
//
//		// -----------------------------------------------
//		// -----------------------------------------------
//		// MOKEPON 2
//		// -----------------------------------------------
//		// -----------------------------------------------
//
//		System.out.println(mikachu.atacs.get(0).nom);
//		MokeponCapturat mikachu2 = mikachu.capturar("MikachuNabo", "David");
//		System.out.println(mikachu2.atacs.get(0).nom);

//		// -----------------------------------------------
//		// -----------------------------------------------
//		// MOKEPON 3
//		// -----------------------------------------------
//		// -----------------------------------------------

//		Mokepon mikachu = new Mokepon("Mikachu", Tipus.AIGUA);
//		mikachu.estatic = "hola";
//		Mokepon mulmasaur = new Mokepon();
//		mulmasaur.estatic = "adeu";
//		System.out.println(mikachu.estatic);
//		
//		MokeponCapturat mikachu2 = mikachu.capturar("MikachuNabo", "David");
//		System.out.println(MokeponCapturat.NumMokeponsCapturats);
//		MokeponCapturat mulmasur2 = mulmasaur.capturar("MikachuNabo", "David");
//		System.out.println(MokeponCapturat.NumMokeponsCapturats);
//		
//		//NumMokeponsCapturats funciona
//		
//		System.out.println(Rasengan.toString());
//		
//		//toString d'atacs funciona
//		
//		System.out.println(mikachu.toString());
//		
//		//toString de mokepons funciona
//		
//		System.out.println(mikachu2.toString());
//		System.out.println(mulmasur2.toString());
//		
//		//toString mokeponCapturat funciona

//	// -----------------------------------------------
//	// -----------------------------------------------
//	// MOKEPON 4
//	// -----------------------------------------------
//	// -----------------------------------------------

		// DONAR A UN POKEMONCAPTURAT UNA POCIÓ I UTILITZARLA
		MokeponCapturat mikachu = new MokeponCapturat("Mikachu", 2, 150, 50, Tipus.FOC);
		MokeponCapturat murmasaur = new MokeponCapturat("Murmasaur", 2, 150, 50, Tipus.AIGUA);
		System.out.println(mikachu.getHp_actual());
		Pocio PujarVida = new Pocio("Reviure:Pocio:PRIMER", 20);
		@SuppressWarnings("unused")
		Pocio BaixarVida = new Pocio("BaixarVida:Pocio:SEGON", -10);
		System.out.println("Mikachu utilitza PujarVida!");
		Atac Nabo = new Atac("Nabo", 2, Tipus.FOC, 10);
		mikachu.afegirAtac(Nabo);
		mikachu.setObjecte(PujarVida);
		PujarVida.utilitzar(mikachu);
		System.out.println("Mikachu ara té " + mikachu.getHp_actual() + " punts de vida");

		Arma Alcala = new Arma("alcala", 20);
		mokedexCapturats.add(murmasaur);
		mokedexCapturats.add(mikachu);

		System.out.println("Combat entre Mikachu i murmasaur");
		hpMoke(mikachu);
		hpMoke(murmasaur);
		System.out.println("Mikachu ataca a murmasaur");
		Alcala.equipar(mikachu);
		mikachu.atacar(murmasaur, 0);
		hpMoke(mikachu);
		hpMoke(murmasaur);

//		// -----------------------------------------------
//		// -----------------------------------------------
//		// MOKEPON 5
//		// -----------------------------------------------
//		// -----------------------------------------------

		System.out.println();

//		try {
//			Ou ous = mikachu.reproduccio(murmasaur);
//		} catch (Exception e) {

//			e.printStackTrace();
//		} finally {
//			System.out.println("Hola");
//		}

		// afegim mokepons a la mokedex

		mokedex.add(murmasaur);
		mokedex.add(mikachu);
		System.out.println(mikachu.getTipus());

		System.out.println(mokedex);
		mokedex.sort(null);
		System.out.println(mokedex);

		System.out.println(murmasaur.getHp_actual());
		mikachu.atacar(murmasaur, 0);
		System.out.println(murmasaur.getHp_actual());

		// per provar combat mokepon

		MokeponCapturat DGGG = new MokeponCapturat("DGGG", 2, 150, 10, Tipus.FOC);
		MokeponCapturat DVVV = new MokeponCapturat("DVVV", 2, 150, 10, Tipus.AIGUA);
		Atac AtacDebil = new Atac("AtacDebil", 5, Tipus.PLANTA, 10);
		Atac AtacMig = new Atac("AtacMig", 10, Tipus.AIGUA, 10);
		Atac AtacFort = new Atac("Nabo", 15, Tipus.FOC, 10);

		DGGG.afegirAtac(AtacFort);
		DGGG.afegirAtac(AtacMig);
		DVVV.afegirAtac(AtacDebil);
		DVVV.afegirAtac(AtacMig);

//		nomDeTots(mokedex);
//		nomDeTots(mokedexCapturats);

//		CombatMokeponEntre(DGGG, DVVV);

//		ObjecteFactory Hola = new ObjecteFactory();
//
//		Hola.crearObjecte("ff");

//		DGGG.reproduccio(DVVV);

//		// -----------------------------------------------
//		// -----------------------------------------------
//		// MOKEPON 7
//		// -----------------------------------------------
//		// -----------------------------------------------
//
//
//		String archivo = "KKK.csv";
//		afegirGimnas("INSTITUT SABADELL", "Mabadell", "Aalcalaa", 2, archivo);
//		afegirGimnas("HERTZALLE", "Berlin", "Franzl Lang", 0, archivo);
//		afegirGimnas("MONGODB", "Mant Quirze", "G. Santama", 1, archivo);
//		afegirGimnas("KONOHA", "País del Foc", "N.S.S", 0, archivo);
//		ja afegits

//		mostrarGimnasos(archivo);
//
//		cercaLider("INSTITUT SABADELL", archivo);
//		cercaLider("HERTZALLE", archivo);
//		cercaLider("MONGODB", archivo);
//		cercaLider("KONOHA", archivo);
//		cercaLider("INSTITUT SABADELL", archivo);
//
//		invictes(5, archivo);

//		canviLider("INSTITUT SABADELL", "Roger", archivo);
//		cercaLider("KONOHA", archivo);
//		afegeixEntrenador("INSTITUT SABADELL", "Truita", archivo);

//		afegirGimnas("INSTITUT", "Sant", "Emilia", 0, archivo);
//		afegeixEntrenador("INSTITUT SABADELL", "EMILIAMILA", archivo);
//		afegeixEntrenador("INSTITUT", "LAIAPALOU", archivo);
//		consultaEntrenadors("INSTITUT", archivo);
		// consultaEntrenadors("INSTITUT SABADELL", archivo);
		// copiaSeguretat("KKK.csv", "KKKCopiaSeguretat.csv");

//		// -----------------------------------------------
//		// -----------------------------------------------
//		// MOKEPON 7
//		// -----------------------------------------------
//		// -----------------------------------------------
//
		@SuppressWarnings("unused")
		Reviure reviure = new Reviure("Hola");

//		String arxiuObjectes = "Objectes\\objecte.dat";
//		String pocioConcreta = "Objectes\\pocions.dat";
		@SuppressWarnings("unused")
		String arxiuMokepon = "Objectes\\mokepon.dat";
//		afegirObjecte(PujarVida, pocioConcreta);
//		afegirObjecte(BaixarVida, pocioConcreta);
//		afegirObjecte(PujarVida, arxiuObjectes);
//		afegirObjecte(BaixarVida, arxiuObjectes);
//		afegirObjecte(reviure, arxiuObjectes);
		@SuppressWarnings("unused")
		Pocio MitjaVida = new Pocio("Mitja:Pocio:TERCER", 25);
		// afegirObjecte(MitjaVida, pocioConcreta);
		System.out.println();
		System.out.println("Pocio recuperada");
//		recuperaObjecte(pocioConcreta);
//		System.out.println();
//
//		ArrayList<Objecte> objectes = recuperaObjectes(arxiuObjectes);
//		System.out.println();
//		System.out.println("ArrayList d'objectes");
//		System.out.println(objectes);
//
//		// Pocio p = recuperaPocioConcreta(arxiuObjectes, 1);
//
//		String archivoMokepon = "Objectes\\mokepon.dat";
//
//		afegeixMokepon(archivoMokepon, DGGG);

//		// -----------------------------------------------
//		// -----------------------------------------------
//		// MOKEPON 8
//		// -----------------------------------------------
//		// -----------------------------------------------

//		// -----------------------------------------------
//		// -----------------------------------------------
//		// MOKEPON 9
//		// -----------------------------------------------
//		// -----------------------------------------------

//		String archiuXML = "entrada.xml";
//		String archiuXMLMokedexAvab = "mokedexAVANCADA.xml";
//
//		Evolucions evo = new Evolucions();
//		Evolucions Super = new Evolucions();
//
//		Entrada entradaProva = new Entrada();
//		ArrayList<String> loc1 = new ArrayList<String>();
//		loc1.add("LOC1");
//		loc1.add("LOC2");
//		entradaProva.id = 25;
//		entradaProva.nom = "Hola";
//		entradaProva.tipus = "Hola";
//		entradaProva.pes = 20;
//		entradaProva.localitzacions = loc1;
//		entradaProva.evolucions = evo;
//
//		Entrada entradaProva2 = new Entrada();
//		ArrayList<String> loc2 = new ArrayList<String>();
//		loc2.add("LOC21");
//		loc2.add("LOC22");
//		entradaProva2.id = 12;
//		entradaProva2.nom = "Bola";
//		entradaProva2.tipus = "Tipus";
//		entradaProva2.pes = 20;
//		entradaProva2.localitzacions = loc2;
//		entradaProva2.evolucions = Super;
//
//		Entrada entradaProva3 = new Entrada();
//		ArrayList<String> loc3 = new ArrayList<String>();
//		loc2.add("LOC31");
//		loc2.add("LOC32");
//		entradaProva3.id = 45;
//		entradaProva3.nom = "TIPUS3";
//		entradaProva3.tipus = "TIPUS3";
//		entradaProva3.pes = 20;
//		entradaProva3.localitzacions = loc2;
//		entradaProva3.evolucions = Super;
//
//		Mokedex mokedexXML = new Mokedex();
//		mokedexXML.nom_entrenador = "MokedexAvancada";
//		mokedexXML.entrades.add(entradaProva);
//		mokedexXML.entrades.add(entradaProva2);
		// mokedexXML.entrades.add(entradaProva3);
		// escriureEntrada(archiuXML, entradaProva);
		// escriureMokedex(archiuXMLMokedexAvab, mokedexXML);
		// llegirMokedex(archiuXMLMokedexAvab);
		// afegirEntradaMokedex(archiuXMLMokedexAvab, entradaProva3);
		// escriureMokedex(archiuXMLMokedexAvab, mokedexXML);
		// afegirEntradaMokedex(archiuXMLMokedexAvab, entradaProva3);
		// modificarPes(archiuXMLMokedexAvab, 25, 10);
		// afegirLocalitzacio(archiuXMLMokedexAvab, 25, "LOCALITZACIONOVA");
		// postEvolucio(archiuXMLMokedexAvab, 01);

//		// -----------------------------------------------
//		// -----------------------------------------------
//		// MOKEPON 10
//		// -----------------------------------------------
//		// -----------------------------------------------

		System.out.println();
		System.out.println("// MOKEPON 10");
		System.out.println();
		System.out.println("escriure");
		// escriureJSONExemple();
		System.out.println();
		System.out.println("llegir");
		// llegirJSON("alumne.json");
		// ENTRADA1 AFEGIR
		JsonObject entrada1 = new JsonObject();
		entrada1.addProperty("id", 2);
		entrada1.addProperty("nom", "Hansueli");
		entrada1.addProperty("pes", 15.2);
		JsonObject evolucions = new JsonObject();
		evolucions.addProperty("pre-evolucio", "null");
		evolucions.addProperty("post-evolucio", "SuperOesch");
		entrada1.add("evolucions", evolucions);
		JsonArray list = new JsonArray();
		list.add("Ruta Hans");
		list.add("Ruta Oesch");
		list.add("Ruta Melanie");
		list.add("Anne Marie");
		entrada1.add("localitzacions", list);

		// afegirAMokedexJSON("mokedexprovaJSON.json", entrada1);

		modificarPesJSON("mok10\\mokedexJSON.json", 1, 1);
		//
	}

	/**
	 * Marca el punt d'inici de les funcions, per major agilitat
	 */

	@SuppressWarnings("unused")
	private static void iniciFuncions() {

	}

	/**
	 * 
	 * Llegeix el fitxer JSON, troba l’entrada amb la id proporcionada, i li suma el
	 * pesExtra al camp pes, i el torna a desar. Si no troba aquesta id, informa de
	 * l’error
	 * 
	 * @param path
	 * @param idEntrada
	 * @param pesExtra
	 * @throws IOException
	 */

	public static void modificarPesJSON(String path, int idEntrada, double pesExtra) throws IOException {
		JsonObject mokedex = null;
		boolean trobat = false;
		try {
			mokedex = llegirJSON(path);
			for (int i = 0; i < mokedex.get("entrades").getAsJsonArray().size(); i++) {
				if (mokedex.get("entrades").getAsJsonArray().get(i).getAsJsonObject().get("id")
						.getAsDouble() == idEntrada) {
					trobat = true;
					mokedex.get("entrades").getAsJsonArray().get(i).getAsJsonObject().addProperty("pes",
							mokedex.get("entrades").getAsJsonArray().get(i).getAsJsonObject().get("pes").getAsDouble()
									+ pesExtra);
					System.out.println("Pes cambiat.");
				}
			}
			if (!trobat) {
				System.out.println("L'entrada amb l'id proporcionada no s'ha trobat.");
			} else {
				System.out.println("Trobat!");
				escriureJSON(path, mokedex);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * Llegeix el fitxer JSON, afegeix la nova entrada al final de l’array entrades,
	 * i el torna a desar.
	 * 
	 * @param nomFitxer
	 * @param novaEntrada
	 * @throws IOException
	 */

	public static void afegirAMokedexJSON(String path, JsonObject novaEntrada) throws IOException {
		JsonObject mokedex = null;
		try {
			mokedex = llegirJSON(path);
			mokedex.get("entrades").getAsJsonArray().add(novaEntrada);
			escriureJSON(path, mokedex);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * Llegeix un fitxer JSON
	 * 
	 * @param path
	 * @return
	 * @throws IOException
	 */

	public static JsonObject llegirJSON(String path) throws IOException {
		JsonObject obj = null;
		try {
			JsonElement arrel = JsonParser.parseReader(new FileReader(path));
			obj = arrel.getAsJsonObject();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return obj;
	}

	/**
	 * 
	 * Escriu un fitxer JSON
	 * 
	 * @param path
	 * @param obj
	 * @throws IOException
	 */

	public static void escriureJSON(String path, JsonObject obj) throws IOException {
		try {
			Gson write = new GsonBuilder().setPrettyPrinting().create();
			write.toJson(obj);
			FileWriter fw = new FileWriter(path);
			fw.append(write.toJson(obj));
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * Llegeix el fitxer XML, troba l’entrada amb la id proporcionada, i diu quina
	 * es la seva post-evolució. Si no troba aquesta id, informa de l’error. Si
	 * post-evolució és un guió, informa que no en té.
	 * 
	 * @param path
	 * @param idEntrada
	 */

	public static void postEvolucio(String path, int idEntrada) {
		File fitxer = new File(path);
		Mokedex mokedex = null;
		boolean idtrobada = false;
		try {
			// Es crea el context indicant la classe arrel
			// hauras de posar la classe adient, clar.
			JAXBContext jaxbContext = JAXBContext.newInstance(Mokedex.class);
			// Es crea un Unmarshaller amb el context de la classe Entrada
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			// Es fa servir el mètode unmarshal, per a obtenir les dades. Sempre s'ha de fer
			// un casteig. fitxerOrigen es una String amb el path al fitxer
			mokedex = (Mokedex) jaxbUnmarshaller.unmarshal(fitxer);
			for (Entrada entrada : mokedex.entrades) {
				if (entrada.id == idEntrada) {
					idtrobada = true;
					System.out.println(
							"La post evolució de l'entrada " + idEntrada + " és " + entrada.evolucions.post_evolucio);
				}
			}
			if (!idtrobada) {
				System.out.println(
						"No s'ha trobat una entrada amb la id especificada, per tant, no hi ha post-evolucio.");
			}
			escriureMokedex(path, mokedex);
		} catch (JAXBException je) {
			je.printStackTrace();
		}
	}

	/**
	 * 
	 * Llegeix el fitxer XML, troba l’entrada amb la id proporcionada, i li afegeix
	 * la nova Localitzacio a la llista de localitzacions, i el torna a desar. Si no
	 * troba aquesta id, informa de l’error
	 * 
	 * @param path
	 * @param idEntrada
	 * @param novaLoc
	 */

	public static void afegirLocalitzacio(String path, int idEntrada, String novaLoc) {
		File fitxer = new File(path);
		Mokedex mokedex = null;
		try {
			// Es crea el context indicant la classe arrel
			// hauras de posar la classe adient, clar.
			JAXBContext jaxbContext = JAXBContext.newInstance(Mokedex.class);
			// Es crea un Unmarshaller amb el context de la classe Entrada
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			// Es fa servir el mètode unmarshal, per a obtenir les dades. Sempre s'ha de fer
			// un casteig. fitxerOrigen es una String amb el path al fitxer
			mokedex = (Mokedex) jaxbUnmarshaller.unmarshal(fitxer);
			for (Entrada entrada : mokedex.entrades) {
				if (entrada.id == idEntrada) {
					entrada.localitzacions.add(novaLoc);
				}
			}
			escriureMokedex(path, mokedex);
		} catch (JAXBException je) {
			je.printStackTrace();
		}
	}

	/**
	 * 
	 * Llegeix el fitxer XML, troba l’entrada amb la id proporcionada, i li suma el
	 * pesExtra al camp pes, i el torna a desar. Si no troba aquesta id, informa de
	 * l’error
	 * 
	 * @param path
	 * @param idEntrada
	 * @param pesExtra
	 */

	public static void modificarPes(String path, int idEntrada, float pesExtra) {
		File fitxer = new File(path);
		Mokedex mokedex = null;
		boolean modificar = false;
		try {
			// Es crea el context indicant la classe arrel
			// hauras de posar la classe adient, clar.
			JAXBContext jaxbContext = JAXBContext.newInstance(Mokedex.class);
			// Es crea un Unmarshaller amb el context de la classe Entrada
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			// Es fa servir el mètode unmarshal, per a obtenir les dades. Sempre s'ha de fer
			// un casteig. fitxerOrigen es una String amb el path al fitxer
			mokedex = (Mokedex) jaxbUnmarshaller.unmarshal(fitxer);
			for (Entrada entrada : mokedex.entrades) {
				if (entrada.id == idEntrada) {
					entrada.pes += (int) pesExtra;
					modificar = true;
				}
			}
			if (modificar) {
				escriureMokedex(path, mokedex);
			}

		} catch (JAXBException je) {
			je.printStackTrace();
		}
	}

	/**
	 * 
	 * Llegeix el fitxer XML, afegeix la nova entrada al final, i el torna a desar
	 * 
	 * @param path
	 * @param novaEntrada
	 */

	public static void afegirEntradaMokedex(String path, Entrada novaEntrada) {
		Mokedex p = llegirMokedex(path);
		p.entrades.add(novaEntrada);
		escriureMokedex(path, p);
	}

	/**
	 * Llegeix mokedex
	 * 
	 * @param path
	 */

	public static Mokedex llegirMokedex(String path) {
		File fitxer = new File(path);
		Mokedex p = null;
		try {
			// Es crea el context indicant la classe arrel
			// hauras de posar la classe adient, clar.
			JAXBContext jaxbContext = JAXBContext.newInstance(Mokedex.class);
			// Es crea un Unmarshaller amb el context de la classe Entrada
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			// Es fa servir el mètode unmarshal, per a obtenir les dades. Sempre s'ha de fer
			// un casteig. fitxerOrigen es una String amb el path al fitxer
			p = (Mokedex) jaxbUnmarshaller.unmarshal(fitxer);
			System.out.println(p);
		} catch (JAXBException je) {
			je.printStackTrace();
		}
		return p;
	}

	/**
	 * 
	 * Escriu mokedex
	 * 
	 * @param path
	 * @param mok
	 */

	public static void escriureMokedex(String path, Mokedex mok) {
		File fitxer = new File(path);
		try {
			// si ja has creat el jaxbContext abans per llegir no cal aquesta línea, clar.
			JAXBContext jaxbContext = JAXBContext.newInstance(Mokedex.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			// Es grava el fitxer desti amb la sortida formatada (aixo ultim s'indica en la
			// instruccio que segueix)
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			// entrada es un objecte de tipus Entrada. fitxerDesti es una String amb el path
			// al fitxer
			jaxbMarshaller.marshal(mok, fitxer);
		} catch (JAXBException je) {
			je.printStackTrace();
		}
	}

	/**
	 * Escriu una entrada
	 * 
	 * @param path
	 * @param entrada
	 */

	public static void escriureEntrada(String path, Entrada entrada) {
		File fitxer = new File(path);
		try {
			// si ja has creat el jaxbContext abans per llegir no cal aquesta línea, clar.
			JAXBContext jaxbContext = JAXBContext.newInstance(Entrada.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			// Es grava el fitxer desti amb la sortida formatada (aixo ultim s'indica en la
			// instruccio que segueix)
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			// entrada es un objecte de tipus Entrada. fitxerDesti es una String amb el path
			// al fitxer
			jaxbMarshaller.marshal(entrada, fitxer);
		} catch (JAXBException je) {
			je.printStackTrace();
		}
	}

	/**
	 * 
	 * Llegeix una entrada
	 * 
	 * @param path
	 */

	public static void llegirEntradaInici(String path) {
		File fitxer = new File(path);
		try {
			// Es crea el context indicant la classe arrel
			// hauras de posar la classe adient, clar.
			JAXBContext jaxbContext = JAXBContext.newInstance(Entrada.class);
			// Es crea un Unmarshaller amb el context de la classe Entrada
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			// Es fa servir el mètode unmarshal, per a obtenir les dades. Sempre s'ha de fer
			// un casteig. fitxerOrigen es una String amb el path al fitxer
			Entrada p = (Entrada) jaxbUnmarshaller.unmarshal(fitxer);
			System.out.println(p);
		} catch (JAXBException je) {
			je.printStackTrace();
		}
	}

	/**
	 * Canvia el nom d’entrenador de tots els Mokepon a “Team Mocket”. No fa res amb
	 * els objectes
	 * 
	 * @param path
	 */
//
	public static void teamMocketAtacaDeNou(String path) throws IOException {
		File fileOriginal = new File(path);
		File fileNova = new File(path + ".temp");
		ArrayList<Object> HOLA = new ArrayList<Object>();

		FileInputStream fis = new FileInputStream(fileOriginal);
		ObjectInputStream ois = new ObjectInputStream(fis);
		FileOutputStream fos = new FileOutputStream(fileNova, true);
		AppendableObjectOutputStream oos = new AppendableObjectOutputStream(fos, true);
		@SuppressWarnings("unused")
		MokeponCapturat mokc = null;
		try {
			while (true) {
				MokeponCapturat o = (MokeponCapturat) ois.readObject();
				o.setNomEntrenador("Team Mocket");
				HOLA.add(o);
			}

		} catch (FileNotFoundException e) {
			for (int i = 0; i < HOLA.size(); i++) {
				oos.writeObject(HOLA.get(i));
			}
			System.out.println("El fitxer no es troba.");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.out.println("No s'ha trobat la classe demanada.");
			e.printStackTrace();
		} catch (EOFException e) {
			e.printStackTrace();
			System.out.println("Final del fitxer.");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

		}
		oos.flush();
		oos.close();
		fos.close();
		ois.close();
		fis.close();
		fileOriginal.delete();
		fileNova.renameTo(fileOriginal);
	}

	/**
	 * 
	 * Recupera un MokeponCapturat amb aquestes dades. Llegirà tot el fitxer, i si
	 * troba un Mokepon amb aquestes dades l’esborrarà i el retornarà.
	 * 
	 * @param path
	 * @param nom
	 * @param nivell
	 * @param entrenador
	 * @param nomDonat
	 * @throws IOException
	 */

	public static MokeponCapturat recuperaMokepon(String path, String nom, int nivell, String entrenador,
			String nomDonat) throws IOException {
		File fileOriginal = new File(path);
		File fileNova = new File(path + ".temp");
		FileInputStream fis = new FileInputStream(fileOriginal);
		ObjectInputStream ois = new ObjectInputStream(fis);
		FileOutputStream fos = new FileOutputStream(fileNova, true);
		AppendableObjectOutputStream oos = new AppendableObjectOutputStream(fos, true);
		MokeponCapturat mokc = null;
		try {
			while (true) {
				Object o = (Object) ois.readObject();

				if (o instanceof MokeponCapturat) {
					// si trobem Mokepon que no compleix característiques donades, no l'escribim
					if (!((MokeponCapturat) o).getNom().equals(nom) && ((MokeponCapturat) o).getNivell() == nivell
							&& ((MokeponCapturat) o).getNomEntrenador().equals(entrenador)
							&& ((MokeponCapturat) o).getNomPosat().equals(nomDonat)) {
						oos.writeObject(o);
					}
				} else {
					mokc = (MokeponCapturat) o;
				}
			}

		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no es troba.");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.out.println("No s'ha trobat la classe demanada.");
			e.printStackTrace();
		} catch (EOFException e) {
			e.printStackTrace();
			System.out.println("Final del fitxer.");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

		}
		oos.flush();
		oos.close();
		fos.close();
		ois.close();
		fis.close();
		fileOriginal.delete();
		fileNova.renameTo(fileOriginal);
		return mokc;
	}

	/**
	 * 
	 * Afegeix un MokeponCapturat al fitxer indicat si no està ja.
	 * 
	 * @param mokC
	 * @throws IOException
	 */

	public static void afegeixMokepon(String path, MokeponCapturat mokC) throws IOException {
		File fileOriginal = new File(path);
		FileInputStream fis = new FileInputStream(fileOriginal);
		ObjectInputStream ois = new ObjectInputStream(fis);
		FileOutputStream fos = new FileOutputStream(fileOriginal, true);
		AppendableObjectOutputStream oos = new AppendableObjectOutputStream(fos, true);
		boolean trobat = false;
		try {
			while (true) {
				Object o = (Object) ois.readObject();
				if (((MokeponCapturat) o).equals(mokC)) {
					trobat = true;
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no es troba.");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.out.println("No s'ha trobat la classe demanada.");
			e.printStackTrace();
		} catch (EOFException e) {
			System.out.println("hola");
			e.printStackTrace();
			System.out.println("Final del fitxer.");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (!trobat) {
				System.out.println("trobat, escrit");
				oos.writeObject(mokC);
			}
			oos.flush();
			fos.close();
			ois.close();
			fis.close();
		}
	}

	/**
	 * 
	 * Retorna una poció que curi exactament n. Si no hi ha cap al fitxer retorna
	 * null.
	 * 
	 * @param n
	 * @return
	 */

	@SuppressWarnings("finally")
	public static Pocio recuperaPocioConcreta(String path, int n) {
		boolean ok = false;
		Pocio p = null;
		try {
			File f = new File(path);
			FileInputStream fis = new FileInputStream(f);
			@SuppressWarnings("resource")
			ObjectInputStream ois = new ObjectInputStream(fis);
			while (true) {
				Object o = (Object) ois.readObject();
				p = (Pocio) o;
				if (p.hp_curada == n) {
					ok = true;
					System.out.println("La pocio recuperada que cura " + n + " es: ");
					System.out.println(p);
				} else {
					ok = false;
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no es troba.");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.out.println("No s'ha trobat la classe demanada.");
			e.printStackTrace();
		} catch (EOFException e) {
			e.printStackTrace();
			System.out.println("Final del fitxer.");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (ok == true) {
				return p;
			} else {
				return null;
			}
		}
	}

	/**
	 * 
	 * Retorna una llista amb tots els objectes
	 * 
	 * @param path
	 */

	public static ArrayList<Objecte> recuperaObjectes(String path) {
		ArrayList<Objecte> objectes = new ArrayList<Objecte>();
		try {
			File f = new File(path);
			FileInputStream fis = new FileInputStream(f);
			@SuppressWarnings("resource")
			ObjectInputStream ois = new ObjectInputStream(fis);
			while (true) {
				//
				Objecte o = (Objecte) ois.readObject();
//				if (o instanceof Pocio) {
//					Pocio p = (Pocio) o;
//					System.out.println(p);
//				} else if (o instanceof Reviure) {
//					Reviure r = (Reviure) o;
//					System.out.println(r);
//				}
				objectes.add(o);
			}
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no es troba.");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.out.println("No s'ha trobat la classe demanada.");
			e.printStackTrace();
		} catch (EOFException e) {
			e.printStackTrace();
			System.out.println("Final del fitxer.");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return objectes;
	}

	/**
	 * 
	 * Retorna el primer objecte trobat
	 * 
	 * @param path
	 */
//
	//
	public static void recuperaObjecte(String path) throws EOFException {
		try {
			File f = new File(path);
			FileInputStream fis = new FileInputStream(f);
			@SuppressWarnings("resource")
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object o = (Object) ois.readObject();
			if (o instanceof Pocio) {
				Pocio p = (Pocio) o;
				System.out.println(p);
			} else if (o instanceof Reviure) {
				Reviure r = (Reviure) o;
				System.out.println(r);
			}
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no es troba.");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.out.println("No s'ha trobat la classe demanada.");
			e.printStackTrace();
		} catch (EOFException e) {
			e.printStackTrace();
			System.out.println("Final del fitxer.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * Afegeix un objecte a l'arxiu
	 * 
	 * @param obj
	 * @param path
	 */

	public static void afegirObjecte(Objecte obj, String path) {
		try {
			//
			File f = new File(path);
			// funciona de forma similar a un fileWriter, amb append inclós
			FileOutputStream fos = new FileOutputStream(f, true);
			AppendableObjectOutputStream oos = new AppendableObjectOutputStream(fos, true);
			oos.writeObject(obj);
			oos.flush();
			oos.close();
			fos.close(); // És molt important tancar bé el fos. Si no el fitxer queda obert
			System.out.println(obj.nom + " afegit al fitxer " + path);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Imprimeix tots els entrenadors que han superat el gimnàs.
	 * 
	 * @param nomGimnas
	 * @param path
	 */

	public static void consultaEntrenadors(String nomGimnas, String path) {
		try {
			File arxiu = new File(path);
			FileReader fr = new FileReader(arxiu);
			BufferedReader br = new BufferedReader(fr);
			while (br.ready()) {
				String arr[] = br.readLine().split(";");
				if (arr[0].equals(nomGimnas)) {
					System.out.println("El gimnas " + nomGimnas + " ha estat superat per:");
					if (arr.length == 0) {
						System.out.println("Aquest gimnas no ha estat superat.");
					} else {
						for (int i = 4; i < arr.length; i++) {
							System.out.println(arr[i]);
						}
					}

				}
			}

			br.close();
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

		}

	}

	/**
	 * Esborra el gimnàs del fitxer.
	 * 
	 * @param nomGimnas
	 * @param path
	 */

	public static void esborrarGimnas(String nomGimnas, String path) {
		try {
			File arxiu = new File(path);
			File arxiuAux = new File(path + ".temp");
			FileReader fr = new FileReader(arxiu);
			BufferedReader br = new BufferedReader(fr);
			FileWriter fw = new FileWriter(arxiuAux);
			BufferedWriter bw = new BufferedWriter(fw);
			while (br.ready()) {
				String arr[] = br.readLine().split(";");
				if (!arr[0].equals(nomGimnas)) {
					bw.append(arr[0] + ";" + arr[1] + ";" + arr[2] + ";" + arr[3] + ";" + arr[4] + "\n");
				}
			}
			bw.flush();
			br.close();
			bw.close();
			fr.close();
			fw.close();
			arxiu.delete();
			arxiuAux.renameTo(arxiu);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

		}
	}

	/**
	 * Augmenta en 1 el nombre d’entrenadors que han superat el gimnàs i afegeix el
	 * nom de l’entrenador al final.
	 * 
	 * @param nomGimnas
	 * @param nomEntrenador
	 * @param path
	 * @throws IOException
	 */

	public static void afegeixEntrenador(String nomGimnas, String nomEntrenador, String path) {
		try {
			File arxiu = new File(path);
			File arxiuAux = new File(path + ".temp");
			FileReader fr = new FileReader(arxiu);
			BufferedReader br = new BufferedReader(fr);
			FileWriter fw = new FileWriter(arxiuAux);
			BufferedWriter bw = new BufferedWriter(fw);
			while (br.ready()) {
				String arr[] = br.readLine().split(";");
				if (arr[0].equals(nomGimnas)) {
					int invictes = Integer.parseInt(arr[3]);
					invictes += 1;
					arr[3] = Integer.toString(invictes);
					for (int i = 0; i < arr.length; i++) {
						bw.append(arr[i] + ";");
					}
					bw.append(nomEntrenador + "\n");
				} else {
					for (int i = 0; i < arr.length; i++) {
						bw.append(arr[i] + ";");
					}
					bw.append("\n");
				}
			}
			bw.flush();
			br.close();
			bw.close();
			fr.close();
			fw.close();
			arxiu.delete();
			arxiuAux.renameTo(arxiu);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

		}
	}

	/**
	 * Permet cambiar el lider del gimnàs indicat a l'arxiu indicat
	 * 
	 * @param nomGimnas
	 * @param nouLider
	 * @param pathArxiu
	 * @param arxiuNou
	 */

	public static void canviLider(String nomGimnas, String nouLider, String path) {
		try {
			File arxiu = new File(path);
			File arxiuAux = new File(path + ".temp");
			FileReader fr = new FileReader(arxiu);
			BufferedReader br = new BufferedReader(fr);
			FileWriter fw = new FileWriter(arxiuAux);
			BufferedWriter bw = new BufferedWriter(fw);
			while (br.ready()) {
				String arr[] = br.readLine().split(";");
				if (arr[0].equals(nomGimnas)) {
					arr[2] = nouLider;
				}
				bw.append(arr[0] + ";" + arr[1] + ";" + arr[2] + ";" + arr[3] + "\n");
			}
			bw.flush();

			br.close();
			bw.close();
			fr.close();
			fw.close();
			System.out.println(arxiu.delete());

			arxiuAux.renameTo(arxiu);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

		}
	}

	/**
	 * Realitza una copia de seguretat d'un arxiu
	 * 
	 * @param path1
	 * @param path2
	 */
	public static void copiaSeguretat(String path1, String path2) throws IOException {
		File arxiu = new File(path1);
		FileReader fr;
		BufferedReader br;
		File arxiuSeguretat = new File(path2);
		FileWriter fw;
		BufferedWriter bw;
		try {
			fw = new FileWriter(arxiuSeguretat);
			bw = new BufferedWriter(fw);
			fr = new FileReader(arxiu);
			br = new BufferedReader(fr);
			while (br.ready()) {
				bw.write(br.readLine() + "\n");
			}
			bw.flush();
			br.close();
			bw.close();
			fr.close();
			fw.close();
			System.out.println("Copia de seguretat amb nom " + path2 + " feta.");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

		}

	}

	/**
	 * Afegeix un gimnàs a l'arxiu especificat. Si s'afegeix un gimnàs ja existent,
	 * llanca una excepció.
	 * 
	 * @param nomGimnas
	 * @param ciutat
	 * @param liderGimnas
	 * @param numEntrenadors
	 * @param path
	 * @throws Exception
	 */
	@SuppressWarnings("resource")
	public static void afegirGimnas(String nomGimnas, String ciutat, String liderGimnas, int numEntrenadors,
			String path) throws Exception {
		File arxiu = new File(path);
		FileWriter fw;
		BufferedWriter bw;
		FileReader fr;
		BufferedReader br;
		try {
			fw = new FileWriter(arxiu, true);
			bw = new BufferedWriter(fw);
			fr = new FileReader(arxiu);
			br = new BufferedReader(fr);
			while (br.ready()) {
				String arr[] = br.readLine().split(";");
				if (arr[0].equals(nomGimnas)) {
					throw new Exception("El Gimnàs ja existeix!");
				}
			}
			bw.write(nomGimnas + ";");
			bw.write(ciutat + ";");
			bw.write(liderGimnas + ";");
			bw.write(numEntrenadors + "\n");
			bw.flush();

			br.close();
			bw.close();
			fr.close();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

		}
	}

	/**
	 * Mostra tots els gimnasos i atributs
	 * 
	 * @throws IOException
	 */
	public static void mostrarGimnasos(String path) throws IOException {
		File f = new File(path);
		FileReader fr;
		BufferedReader br;
		try {
			fr = new FileReader(f);
			br = new BufferedReader(fr);
			System.out.println();
			System.out.println("Els gimnasos existens són els següents:");
			System.out.println();
			while (br.ready()) {
				System.out.println(br.readLine());
			}
			System.out.println();

			br.close();
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// cercaLider(String nomGimnas). Retorna el nom del líder del gimnàs
	/**
	 * Mostra el lider del gimnas indicat
	 * 
	 * @param nomGimnas
	 * @param arxiuLlegir
	 */
	public static void cercaLider(String nomGimnas, String arxiuLlegir) {
		File f = new File(arxiuLlegir);
		FileReader fr;
		BufferedReader br;
		try {
			fr = new FileReader(f);
			br = new BufferedReader(fr);
			System.out.println();
			while (br.ready()) {
				String arr[] = br.readLine().split(";");
				if (arr[0].equals(nomGimnas)) {
					System.out.println("El gimnas " + arr[0]);
					System.out.println("té com a entrenador a " + arr[2]);
					System.out.println();
				}
			}
			br.close();
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

//
//
	/**
	 * Mostra els gimnasos superat més de n vegades
	 * 
	 * @param n
	 * @param arxiuLlegir
	 */
	public static void invictes(int n, String arxiuLlegir) {
		File f = new File(arxiuLlegir);
		FileReader fr;
		BufferedReader br;
		try {
			fr = new FileReader(f);
			br = new BufferedReader(fr);
			System.out.println("Reader carregat.");
			System.out.println();
			while (br.ready()) {
				String arr[] = br.readLine().split(";");
				if (Integer.parseInt(arr[3]) < n) {
					System.out.println("El líder del gimnàs " + arr[0] + " ha estat superat " + arr[3] + " vegades.");
				}
			}
			br.close();
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void imprimirArray(String[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i] + " ");
		}
	}

	public static void nomDeTots(ArrayList<? extends Mokepon> list) {
		for (Mokepon m : list) {
			m.diguesNom();
		}
	}

	public static void CombatMokeponEntre(MokeponCapturat m1, MokeponCapturat m2) {

		System.out.println("Combat entre " + m1.getNom() + " i " + m2.getNom());
		System.out.println(m1.getNom() + " te " + m1.getHp_actual() + " punts de vida");
		System.out.println(m2.getNom() + " te " + m2.getHp_actual() + " punts de vida");
		Mokepon[] combatients = new Mokepon[2];
		combatients[0] = m1;
		combatients[1] = m2;
		int torn = MokeponMesRapid(m1, m2);
		boolean CombatAcabat = false;
		while (!CombatAcabat) {
			int numAtac = triaAtac(combatients[torn]);
			CombatAcabat = atacar(m1, m2, torn, numAtac);
			torn = seguentTorn(torn);
		}
		if (m1.isDebilitat()) {
			// guanya 2
			System.out.println(m2.getNom() + " ha guanyat!");
		} else {
			// guanya 1
			System.out.println(m1.getNom() + " ha guanyat!");
		}

	}

	public static boolean atacar(Mokepon m1, Mokepon m2, int turn, int numAtac) {
		if (turn == 0) {
			m1.atacar(m2, numAtac);
			System.out.println(m1.getNom() + " ataca a " + m2.getNom());
			System.out.println(m2.getNom() + " te " + m2.getHp_actual() + " punts de vida.");
			return m2.isDebilitat();

		} else {
			m2.atacar(m1, numAtac);
			System.out.println(m2.getNom() + " ataca a " + m1.getNom());
			System.out.println(m1.getNom() + " te " + m1.getHp_actual() + " punts de vida.");
			return m1.isDebilitat();
		}

	}

	public static int seguentTorn(int torn) {
		if (torn == 0) {
			return 1;
		} else {
			return 0;
		}
	}

	public static int MokeponMesRapid(MokeponCapturat m1, MokeponCapturat m2) {
		Random r = new Random();
		if (m1.getVel() > m2.getVel()) {
			return 0;
		} else if (m1.getVel() < m2.getVel()) {
			return 1;
		} else {
			int random = r.nextInt(0, 2);
			if (random == 1) {
				return 0;
			} else {
				return 1;
			}
		}

	}

	public static int triaAtac(Mokepon mok) {
		System.out.println("Escull l'atac " + mok.getNom() + "!");
		mok.getAtacs();
		int atac = sc.nextInt();
		return atac;
	}

	public static void hpMoke(MokeponCapturat mok) {
		System.out.println(mok.getNom() + " té " + mok.getHp_actual() + " punts de vida!");
	}

}
