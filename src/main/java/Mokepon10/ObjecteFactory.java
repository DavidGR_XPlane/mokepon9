package Mokepon10;

public class ObjecteFactory {
//
	public Objecte crearObjecte(String tipus) {
		if (tipus == null || tipus.isEmpty())
			return null;
		switch (tipus) {
		case "POCIO":
			return new Pocio(tipus, 50); // les pocions per defecte curen 50 de vida
		case "SUPERPOCIO":
			return new Pocio(tipus, 100);
		case "HIPERPOCIO":
			return new Pocio(tipus, 200);
		case "ARMA":
			return new Arma(tipus, 50);
		case "SUPERARMA":
			return new Arma(tipus, 75);
		case "HIPERARMA":
			return new Arma(tipus, 100);
		case "ARMADURA":
			return new Arma(tipus, 50);
		case "SUPERARMADURA":
			return new Arma(tipus, 75);
		case "HIPERARMADURA":
			return new Arma(tipus, 100);
		default:
			throw new IllegalArgumentException("Tipus d'objecte desconegut " + tipus);
		}
	}

}
