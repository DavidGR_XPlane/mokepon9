package Mokepon10;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "evolucions")
@XmlType(propOrder = { "pre_evolucio", "post_evolucio" })

public class Evolucions {

	String pre_evolucio;
	String post_evolucio;

	public Evolucions() {
		this.pre_evolucio = "DEFAULT";
		this.post_evolucio = "DEFAULT";
	}
	
	@XmlElement(name = "pre_evolucio")
	public String getPre_evolucio() {
		return pre_evolucio;
	}

	public void setPre_evolucio(String pre_evolucio) {
		this.pre_evolucio = pre_evolucio;
	}

	@XmlElement(name = "post_evolucio")
	public String getPost_evolucio() {
		return post_evolucio;
	}

	public void setPost_evolucio(String post_evolucio) {
		this.post_evolucio = post_evolucio;
	}

	@Override
	public String toString() {
		return "Evolucions [pre_evolucio=" + pre_evolucio + ", post_evolucio=" + post_evolucio + "]";
	}

}