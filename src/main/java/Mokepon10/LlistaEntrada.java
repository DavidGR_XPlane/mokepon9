package Mokepon10;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class LlistaEntrada {

	List<Entrada> entrades = new ArrayList<>();

	// l'element XML que fa d'embolcall es diu entrades
	@XmlElementWrapper(name = "entrades")
	// cada element individual es diu entrada
	@XmlElement(name = "entrada")
	public List<Entrada> getEntrades() {
		return entrades;
	}

}
