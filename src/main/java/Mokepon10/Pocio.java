package Mokepon10;

import java.io.Serializable;

public class Pocio extends Objecte implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6159783139552654771L;

	int hp_curada;

	Pocio(String nom, int hp_curada) {
		super(nom);
		this.nom = nom;
		this.hp_curada = hp_curada;
	}

	public void utilitzar(MokeponCapturat mok) {
		if (!mok.isDebilitat()) {
			int vidaActualMok = mok.getHp_actual();
			int vidaQueSumaPocio = this.hp_curada;
			int vidaMaximaMok = mok.getHp_max();
			if ((vidaActualMok + vidaQueSumaPocio) > vidaMaximaMok) {
				mok.setHp_actual(mok.getHp_max());
			} else {
				mok.setHp_actual(this.hp_curada + mok.getHp_actual());
			}
			this.Quantitat--;
		} else {
			System.out.println(mok.getNom() + " està debilitat!");
		}

	}

	@Override
	public String toString() {
		return "Pocio [hp_curada=" + hp_curada + ", nom=" + nom + ", Quantitat=" + Quantitat + "]";
	}

}
