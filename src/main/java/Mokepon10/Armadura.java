package Mokepon10;

public class Armadura extends Objecte implements Equipament {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8185434177684583963L;
	int defensaExtra;
//
	public Armadura(String nom, int defensa) {
		super(nom);
		this.nom = nom;
		this.defensaExtra = defensa;

	}

	public void equipar(MokeponCapturat mok) {
		mok.setObjecteEquipat(this);
		// afegeix a l'atac del pokemon el dany de l'atac equipat
		mok.setDef(mok.getDef() + this.defensaExtra);
		System.out.println(mok.getNom() + " ha equipat " + this.nom);
	}

	public void desequipar(MokeponCapturat mok) {
		mok.setDef(mok.getDef() - this.defensaExtra);
		mok.setObjecteEquipat(null);
		System.out.println(mok.getNom() + " ha desequipat " + this);
	}

	public void utilitzar(MokeponCapturat mok) {
		equipar(mok);
	}

}
