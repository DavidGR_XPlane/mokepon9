package Mokepon10;

import java.util.ArrayList;

public class ArrayListAcotada<T> extends ArrayList<T> {
//
	private static final long serialVersionUID = 1L;

	@Override
	public T get(int index) {
		if (index > super.size()) {
			return super.get(super.size() - 1);
		} else if (index < super.size()) {
			return super.get(0);
		} else {
			return super.get(index);
		}
	}

}
