package Mokepon10;

public class Parella<T, U> {
//
	private T primer;
	private T segon;

	public Parella(T t, T t2) {
		this.primer = t;
		this.segon = t2;
		
	}
	
	public T getPrimer() {
		return primer;
	}

	public void setPrimer(T primer) {
		this.primer = primer;
	}

	public T getSegon() {
		return segon;
	}

	public void setSegon(T segon) {
		this.segon = segon;
	}

}
