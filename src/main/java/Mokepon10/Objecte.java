package Mokepon10;

import java.io.Serializable;

public abstract class Objecte implements Serializable {
	private static final long serialVersionUID = -3117330509849981164L;
	String nom;
	int Quantitat;

	public Objecte(String nom) {

		this.nom = nom;
		this.Quantitat = 1;

	}

	public void obtenir(int numObjectes) {

		this.Quantitat++;

	}

	public void donar(MokeponCapturat mokC) {

		// a Objecte fes un mètode “donar” que li passes
		// un MokeponCapturat, i posa el propi objecte (this)
		// com a l’atribut objecte de MokeponCapturat

		mokC.setObjecte(this);

	}

	public abstract void utilitzar(MokeponCapturat mok);

	public String getNom() {
		return nom;
	}

	public int getQuantitat() {
		return Quantitat;
	}

}
