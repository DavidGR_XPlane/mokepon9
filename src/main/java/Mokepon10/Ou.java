package Mokepon10;

import java.util.Random;

public class Ou {
//
	private String especie;
	private Tipus tipus;
	private int passesRestants;

	public Ou(String especie, Tipus tipus) {
		Random r = new Random();
		this.especie = especie;
		this.tipus = tipus;
		this.passesRestants = r.nextInt(5, 11);

	}
	
	public void caminar() {
		this.passesRestants--;
		if (this.passesRestants == 0) {
			eclosionar();
		}
	}

	public Mokepon eclosionar() {
		Mokepon Mok = new Mokepon(this.especie, this.tipus);
		return Mok;
	}

}
