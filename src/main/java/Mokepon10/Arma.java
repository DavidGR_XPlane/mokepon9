package Mokepon10;


public class Arma extends Objecte implements Equipament {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8413085038038209601L;
	int atacExtra;
//
	Arma(String nom, int atacExtra) {
		super(nom);
		this.nom = nom;
		this.atacExtra = atacExtra;

	}

	public void equipar(MokeponCapturat mok) {
		mok.setObjecteEquipat(this);
		// afegeix a l'atac del pokemon el dany de l'atac equipat
		mok.setAtk(mok.getAtk() + this.atacExtra);
		System.out.println(mok.getNom() + " ha equipat " + this.nom);
	}

	public void desequipar(MokeponCapturat mok) {
		mok.setAtk(mok.getAtk() - this.atacExtra);
		mok.setObjecteEquipat(null);
		System.out.println(mok.getNom() + " ha desequipat " + this);
	}

	public void utilitzar(MokeponCapturat mok) {
		equipar(mok);
	}

}
