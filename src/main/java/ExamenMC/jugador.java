package ExamenMC;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "jugador")
@XmlType(propOrder = { "nom", "pes", "dades", "nevera" })

public class jugador {

	String nom;
	double pes;
	dades dades;
	List<String> nevera = new ArrayList<>();

	public jugador() {
		super();
	}

	@XmlElement(name = "nom")
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@XmlElement(name = "pes")
	public double getPes() {
		return pes;
	}

	public void setPes(double pes) {
		this.pes = pes;
	}

	@XmlElement(name = "dades")
	public dades getDades() {
		return dades;
	}

	public void setDades(dades dades) {
		this.dades = dades;
	}

	@XmlElementWrapper(name = "nevera")
	@XmlElement(name = "aliment")
	public List<String> getNevera() {
		return nevera;
	}

	public void setNevera(List<String> nevera) {
		this.nevera = nevera;
	}

	@Override
	public String toString() {
		return "jugador [nom=" + nom + ", pes=" + pes + ", dades=" + dades + ", nevera=" + nevera + "]";
	}

}
