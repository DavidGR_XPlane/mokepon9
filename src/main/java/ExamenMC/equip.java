package ExamenMC;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "equip")
@XmlType(propOrder = { "nom", "jugadors" })

public class equip {

	String nom;
	List<jugador> jugadors = new ArrayList<>();

	public equip() {
		super();
	}

	@XmlElement(name = "nom")
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@XmlElementWrapper(name = "jugadors")
	@XmlElement(name = "jugador")
	public List<jugador> getJugadors() {
		return jugadors;
	}

	public void setJugadors(List<jugador> jugadors) {
		this.jugadors = jugadors;
	}

	@Override
	public String toString() {
		return "equip [nom=" + nom + ", jugadors=" + jugadors + "]";
	}

}
