package ExamenMC;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class mainmc {

	static String menjarMCXML = "mc\\menjarMC.xml";
	static String jugadorMCXML = "mc\\jugadorMC.xml";
	static String menjarMCMESXML = "mc\\menjarMCmes.xml";
	static String menjarMC2XML = "mc\\menjarMC2.xml";
	static String jugadorMCJSON = "mc\\jugadorMC.json";
	static String menjarMCJSON = "mc\\menjarMC.json";
	static String menjarMC3JSON = "mc\\menjarMC3.json";
	static String menjarMCMESJSON = "mc\\menjarMCmes.json";
	static String barreja = "mc\\menjarMCBarreja.json";

	public static void main(String[] args) throws IOException {

		// llegirEquipXML(menjarMCXML);
		// ArrayList<jugador> jugadors = alineacio(menjarMCXML);
		// System.out.println(jugadors);
		// incorporarJugador(jugadorMCXML);
		// assegurarDieta(menjarMCXML, "Fish and chips");
		// llegirJSON(jugadorMCJSON);

		// alineacioJSON(menjarMCJSON);
		// controlMedic(menjarMCJSON);
		// assegurarDietaJSON(menjarMCJSON, "Pastís de Carn", menjarMC3JSON);
		// incoroporarJugadorJSON();

		// barreja();

	}

	/**
	 * 
	 * 3.1 Crea la funció barreja() que llegeixi l’arxiu “menjarMC.json” i també
	 * l’arxiu “jugador.xml”, i incorpori aquest jugador a l’equip. Has de guardar
	 * el resultat a “menjarMCBarreja.json”.
	 * 
	 * INCOMPLETA INCOMPLETA INCOMPLETA
	 * 
	 * @throws IOException
	 * 
	 */

	public static void barreja() throws IOException {
		JsonObject equip = llegirJSON(menjarMCJSON);
		jugador jugador = llegirJugadorXML(jugadorMCXML);
		equip.get("jugadors").getAsJsonArray().getAsJsonObject().addProperty("nom", jugador.nom);
		equip.get("jugadors").getAsJsonArray().getAsJsonObject().addProperty("pes", jugador.pes);
		equip.get("jugadors").getAsJsonArray().getAsJsonObject().get("dades").getAsJsonObject().addProperty("dorsal",
				jugador.dades.dorsal);
		equip.get("jugadors").getAsJsonArray().getAsJsonObject().get("dades").getAsJsonObject().addProperty("posicio",
				jugador.dades.posicio);

		escriureJSON(barreja, equip);
	}

	@SuppressWarnings("unused")
	private static void FUNCIONSJSON() {

	}

	/**
	 * Llegeix l’arxiu “jugadorMC.json” i també llegeix a l’estructura de l’arxiu
	 * “menjarMC.json”, afegeix el jugador a l’estructura, i el guarda a l’arxiu
	 * “menjarMCmes.json” (1p).
	 * 
	 * @throws IOException
	 * 
	 */

	public static void incoroporarJugadorJSON() throws IOException {
		JsonObject jugador = llegirJSON(jugadorMCJSON);
		JsonObject equip = llegirJSON(menjarMCJSON);
		equip.get("jugadors").getAsJsonArray().add(jugador);
		escriureJSON(menjarMCMESJSON, equip);
	}

	/**
	 * 
	 * Repassa les neveres de tots els jugadors, i afegeix a tothom que li falti,
	 * l’aliment passat per paràmetre. Un cop tothom té el rebost ho guarda a
	 * l’arxiu “menjarMC3.json”
	 * 
	 * @throws IOException
	 * 
	 */

	public static void assegurarDietaJSON(String equippath, String aliment, String aux) throws IOException {
		JsonObject equip = llegirJSON(equippath);
		boolean parametretrobat;
		System.out.println();
		System.out.println();
		// per cada jugador
		for (int i = 0; i < equip.get("jugadors").getAsJsonArray().size(); i++) {
			parametretrobat = false;
			// es mira tota la nevera
			for (int j = 0; j < equip.get("jugadors").getAsJsonArray().get(i).getAsJsonObject().get("nevera")
					.getAsJsonArray().size(); j++) {
				// mirem si hi ha el aliment passat per paràmetre
				if ((equip.get("jugadors").getAsJsonArray().get(i).getAsJsonObject().get("nevera").getAsJsonArray()
						.get(j).getAsString().equals(aliment))) {
					parametretrobat = true;
				}
			}
			if (!parametretrobat) {
				equip.get("jugadors").getAsJsonArray().get(i).getAsJsonObject().get("nevera").getAsJsonArray()
						.add(aliment);
				escriureJSON(aux, equip);
			}

		}

	}

	/**
	 * 
	 * Revisa l’estat dels jugadors. Per aquells que pesin més de 80kg, els ha de
	 * donar advertències per pantalla, que seran: Si té “cervesa” a la nevera ha de
	 * dir: [Nom jugador] elimina la cervesa Si no té “amanida” a la nevera ha de
	 * dir: [Nom jugador] et falta amanida
	 * 
	 * @throws IOException
	 * 
	 */

	public static void controlMedic(String equippath) throws IOException {
		JsonObject equip = llegirJSON(equippath);
		System.out.println();
		System.out.println();
		boolean cervesatrobada;
		boolean amanidatrobada;
		for (int i = 0; i < equip.get("jugadors").getAsJsonArray().size(); i++) {
			cervesatrobada = false;
			amanidatrobada = false;
			if (equip.get("jugadors").getAsJsonArray().get(i).getAsJsonObject().get("pes").getAsDouble() > 80) {
				// mira tota la nevera
				for (int j = 0; j < equip.get("jugadors").getAsJsonArray().get(i).getAsJsonObject().get("nevera")
						.getAsJsonArray().size(); j++) {
					// System.out.println(equip.get("jugadors").getAsJsonArray().get(i).getAsJsonArray().get(j).);
					System.out.println(equip.get("jugadors").getAsJsonArray().get(i).getAsJsonObject().get("nevera")
							.getAsJsonArray().get(j));
					// SI HI HA UNA CERVESA
					if (equip.get("jugadors").getAsJsonArray().get(i).getAsJsonObject().get("nevera").getAsJsonArray()
							.get(j).getAsString().equals("cervesa")) {
						cervesatrobada = true;
					}
					// SI HI HA AMANIDA
					if (equip.get("jugadors").getAsJsonArray().get(i).getAsJsonObject().get("nevera").getAsJsonArray()
							.get(j).getAsString().equals("amanida")) {
						amanidatrobada = true;
					}
				}
			}

			if (cervesatrobada == true) {
				System.out.println(
						equip.get("jugadors").getAsJsonArray().get(i).getAsJsonObject().get("nom").getAsString()
								+ " elimina la cervesa.");
			}
			if (!amanidatrobada) {
				System.out.println(
						equip.get("jugadors").getAsJsonArray().get(i).getAsJsonObject().get("nom").getAsString()
								+ " et falta amanida.");
			}
		}
	}

	/**
	 * 
	 * llegeix l’arxiu “menjarMC.json” i escriu per pantalla el nom dels jugadors
	 * amb el seu pes i el seu dorsal.
	 * 
	 * @param equippath
	 * @throws IOException
	 */

	public static void alineacioJSON(String equippath) throws IOException {
		JsonObject equip = llegirJSON(equippath);
		System.out.println();
		for (int i = 0; i < equip.get("jugadors").getAsJsonArray().size(); i++) {
			System.out
					.println(equip.get("jugadors").getAsJsonArray().get(i).getAsJsonObject().get("nom").getAsString());
			System.out
					.println(equip.get("jugadors").getAsJsonArray().get(i).getAsJsonObject().get("pes").getAsDouble());
			System.out.println(equip.get("jugadors").getAsJsonArray().get(i).getAsJsonObject().get("dades")
					.getAsJsonObject().get("dorsal").getAsString());
			System.out.println();
		}

	}

	/**
	 * 
	 * Escriu un objecte JSON al path indicat
	 * 
	 * @param path
	 * @param obj
	 * @throws IOException
	 */

	public static void escriureJSON(String path, JsonObject obj) throws IOException {
		try {
			Gson write = new GsonBuilder().setPrettyPrinting().create();
			write.toJson(obj);
			FileWriter fw = new FileWriter(path);
			fw.append(write.toJson(obj));
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * Llegeix un JSON
	 * 
	 * @param path
	 * @return
	 * @throws IOException
	 */

	public static JsonObject llegirJSON(String path) throws IOException {
		JsonObject obj = null;
		try {
			JsonElement arrel = JsonParser.parseReader(new FileReader(path));
			obj = arrel.getAsJsonObject();
			System.out.println(obj);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return obj;
	}

	@SuppressWarnings("unused")
	private static void FUNCIONSXML() {

	}

	/**
	 * Llegeix les neveres de tots els jugadors, i afegeix a tothom que li falti,
	 * l’aliment passat per paràmetre. Som a Anglaterra! Així que fes una crida des
	 * del main per assegurar que tothom tingui “Fish and chips”.
	 * 
	 * @param pathequip
	 * @param aliment
	 */

	public static void assegurarDieta(String pathequip, String aliment) {
		File fitxer = new File(menjarMC2XML);
		equip equip = llegirEquipXML(menjarMCXML);
		boolean trobat = false;
		for (jugador jug : equip.jugadors) {
			trobat = false;
			for (int i = 0; i < jug.nevera.size(); i++) {
				if (jug.nevera.get(i).equals(aliment)) {
					trobat = true;
				}
			}
			if (!trobat) {
				jug.nevera.add(aliment);
			}
		}
		escriureEquipXML(fitxer, equip);
	}

	/**
	 * 
	 * Llegeix l’arxiu “jugadorMC.xml” i també llegeix a l’estructura de l’arxiu
	 * “menjarMC.xml”, afegeix el jugador a l’estructura, i el guarda a l’arxiu
	 * “menjarMCmes.xml”
	 * 
	 * @param path
	 */

	public static void incorporarJugador(String path) {
		File fitxer = new File(menjarMCMESXML);
		jugador jugador = llegirJugadorXML(path);
		equip equip = llegirEquipXML(menjarMCXML);
		equip.jugadors.add(jugador);
		escriureEquipXML(fitxer, equip);
	}

	/**
	 * llegeix l’arxiu “menjarMC.xml” i escriu per pantalla el nom dels jugadors amb
	 * el seu dorsal i posició. Retorna una llista de jugadors.
	 * 
	 * @param path
	 * @return ArrayList<jugador> alineacio
	 */

	public static ArrayList<jugador> alineacio(String path) {
		equip equip = llegirEquipXML(path);
		System.out.println("Alineació:");
		ArrayList<jugador> alineacio = new ArrayList<jugador>();
		for (int i = 0; i < equip.jugadors.size(); i++) {
			System.out.println(equip.jugadors.get(i).nom);
			System.out.println(equip.jugadors.get(i).dades.dorsal);
			System.out.println(equip.jugadors.get(i).dades.posicio);
			alineacio.add(equip.jugadors.get(i));
		}
		return alineacio;
	}

	/**
	 * 
	 * Escriu un equip XML
	 * 
	 * @param fitxer
	 * @param equip
	 */

	public static void escriureEquipXML(File fitxer, equip equip) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(equip.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(equip, fitxer);
		} catch (JAXBException je) {
			je.printStackTrace();
		}
	}

	/**
	 * 
	 * Llegeix un equip XML
	 * 
	 * @param path
	 * @return
	 */

	public static equip llegirEquipXML(String path) {
		File fitxer = new File(path);
		equip equip = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(equip.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			equip = (equip) jaxbUnmarshaller.unmarshal(fitxer);
			System.out.println(equip);
		} catch (JAXBException je) {
			je.printStackTrace();
		}
		return equip;
	}

	public static jugador llegirJugadorXML(String path) {
		File fitxer = new File(path);
		jugador jugador = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(jugador.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			jugador = (jugador) jaxbUnmarshaller.unmarshal(fitxer);
			System.out.println(jugador);
		} catch (JAXBException je) {
			je.printStackTrace();
		}
		return jugador;
	}

}
