package ExamenMC;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "dades")
@XmlType(propOrder = { "dorsal", "posicio" })

public class dades {

	double dorsal;
	String posicio;

	public dades() {
		super();
	}

	@XmlElement(name = "dorsal")
	public double getDorsal() {
		return dorsal;
	}

	public void setDorsal(double dorsal) {
		this.dorsal = dorsal;
	}

	@XmlElement(name = "posicio")
	public String getPosicio() {
		return posicio;
	}

	public void setPosicio(String posicio) {
		this.posicio = posicio;
	}

	@Override
	public String toString() {
		return "dades [dorsal=" + dorsal + ", posicio=" + posicio + "]";
	}

}
